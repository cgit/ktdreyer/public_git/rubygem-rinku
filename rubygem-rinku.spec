%global gem_name rinku

Name: rubygem-%{gem_name}
Version: 1.7.3
Release: 4%{?dist}
Summary: Autolinking for Ruby
Group: Development/Languages
License: ISC
URL: https://github.com/vmg/rinku
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# Minitest 5 compat
# https://github.com/vmg/rinku/pull/48
Patch0: rinku-1.7.3-minitest.patch
%if 0%{?el7}
Requires: ruby
Requires: ruby(rubygems)
%endif
BuildRequires: rubygems-devel
BuildRequires: ruby-devel
BuildRequires: rubygem(minitest)
%if 0%{?el7}
Provides: rubygem(%{gem_name}) = %{version}
%endif

%description
A fast and very smart autolinking library that acts as a drop-in replacement
for Rails `auto_link`.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%autosetup -p 1 -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Remove unnecessary Rakefile
rm Rakefile
sed -i "s|\"Rakefile\",||g" %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%gem_install

# Remove unnecessary gemspec file
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
       %{buildroot}%{gem_dir}/

# Remove deprecated "ext" directory
rm -r %{buildroot}%{gem_instdir}/ext

# Move the binary extension
mkdir -p %{buildroot}%{gem_extdir_mri}
cp -pa .%{gem_extdir_mri}/* %{buildroot}%{gem_extdir_mri}/

%check
pushd .%{gem_instdir}
  ruby -I"lib:%{buildroot}%{gem_extdir_mri}" test/autolink_test.rb
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/COPYING
%doc %{gem_instdir}/README.markdown
%{gem_libdir}
%{gem_extdir_mri}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%exclude %{gem_instdir}/test

%changelog
* Fri Jan 08 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.7.3-4
- Drop macros for Fedora 20 (now EOL)
- Switch to %%autosetup macro
- Drop unneeded %%license definition

* Sat Mar 28 2015 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.7.3-3
- Drop Fedora 19 conditionals
- Remove extraneous BR: ruby. This is covered by BR: ruby-devel.
- Patch for Minitest 5
- Use %%license macro

* Thu Jul 10 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.7.3-2
- Adjustments for https://fedoraproject.org/wiki/Changes/Ruby_2.1

* Wed Feb 05 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 1.7.3-1
- Initial package
